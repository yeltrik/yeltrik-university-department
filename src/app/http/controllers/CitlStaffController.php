<?php

namespace App\Http\Controllers;

use App\CitlStaff;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;

class CitlStaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', CitlStaff::class);

        $citlStaffs = CitlStaff::query()
            ->select(['citl_staff.*'])
            ->join('wku_identities as wi', function($join){
                $join->on('wi.id', '=', 'citl_staff.wku_identity_id');
            })
            ->orderBy('wi.name', 'asc')
            ->paginate(15);

        return view('citl-staff.index', compact('citlStaffs'));
    }

    /**
     * Display the specified resource.
     *
     * @param CitlStaff $citlStaff
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function show(CitlStaff $citlStaff)
    {
        $this->authorize('view', $citlStaff);

        return view('citl-staff.show', compact('citlStaff'));
    }

}
