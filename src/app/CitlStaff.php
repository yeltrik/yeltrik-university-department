<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CitlStaff extends Model
{
    CONST PROJECT_GID = '1166780300767705';

    public function getTitleAttribute()
    {
        return $this->wkuIdentity->title;
    }

    public function wkuIdentity()
    {
        return $this->belongsTo(WkuIdentity::class);
    }

}
